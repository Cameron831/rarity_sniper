package NFT;

import com.jaunt.*;

import java.util.TreeMap;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

public class main {
    public static void main(String[] args) throws ResponseException, InterruptedException {
        TreeMap<Double, NFT> rarities = new TreeMap<>();
        String url = "https://opensea.io/assets/0xbc4ca0eda7647a8ab7c2061c2e118a18a936f13d/";
        int i = 1;
        while (i < 30) {
            String link = url + i;
            Thread newThread = new Thread(() -> {
                NFT temp = new NFT(link);
                try {
                    if (temp.forSale()) {
                        //TimeUnit.SECONDS.sleep(1);
                        temp.findTraits();
                        //TimeUnit.SECONDS.sleep(1);
                        temp.findRarity();
                    }
                } catch (ResponseException e) {
                    e.printStackTrace();
                }
                System.out.println("Running Thread: " + Thread.currentThread());
                System.out.println("Thread Count: " + Thread.activeCount());
                rarities.put(temp.getRarity(), temp);
            });
            newThread.start();
            i++;
        }
        while(Thread.activeCount() > 3) {
            //System.out.println("Processing...");
            //TimeUnit.SECONDS.sleep(1);
        }
        System.out.println(rarities);
        System.out.println(rarities.get(rarities.lastKey()).getLink());
        /*
        Set<Double> keys = rarities.keySet();
        for(Double key: keys) {
            System.out.println(key);
        }

         */
        //TimeUnit.SECONDS.sleep(15);
        //System.out.println(rarities.get(rarities.lastKey()).getLink());
    }
}
