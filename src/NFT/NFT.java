package NFT;

import java.util.ArrayList;
import com.jaunt.*;

public class NFT {
    private double rarity;
    private String link;
    private ArrayList<Double> traits = new ArrayList<>();

    public NFT(String link) {
        this.link = link;
    }

    public boolean forSale() throws ResponseException {
        UserAgent agent = new UserAgent();
        agent.setProxyHost("http://scraperapi131d5f3b6bc38b46e4f98c679503f848@proxy-server.scraperapi.com");
        agent.setProxyPort(8001);
        agent.visit(this.link);
        Elements buttons = agent.doc.findEvery("<button");
        for(Element button: buttons) {
            String test = button.innerHTML();
            if(test.contains("Buy now")) {
                return true;
            }
        }
        return false;
    }

    public void findTraits() throws ResponseException {
        UserAgent agent = new UserAgent();
        agent.visit(this.link);
        Elements attributes = agent.doc.findEvery("<div class=Property--rarity>").findEvery("<div>");
        for(Element attribute: attributes) {
            String temp = attribute.innerHTML().split("%")[0];
            double trait = Double.parseDouble(temp);
            this.traits.add(trait);
        }
    }

    public void printTraits() {
        for(int i = 0; i < this.traits.size()/2; i++) {
            System.out.println(this.traits.get(i));
        }
    }

    public void findRarity() {
        ArrayList<Double> fractional = new ArrayList<>();
        for(Double x: this.traits) {
            fractional.add(x/100);
        }
        this.traits.clear();
        for(Double x: fractional) {
            this.traits.add(1 / x);
        }
        for(Double x: this.traits) {
            this.rarity += x;
        }
    }

    public void process() throws ResponseException {
        if(this.forSale()) {
            this.findTraits();
            this.findRarity();
        }
    }

    public double getRarity() {return this.rarity;}
    public String getLink() {return this.link;}

}
